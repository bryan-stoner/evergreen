module.exports = {

  name: "idfive",

  sansSerif: "Montserrat",
  serif: "Lora",
  slab: null,

  bodySize: 19,
  bodyLine: 23,
  bodyColor: '#fff',

  h1Size: 65,
  h1Line: 65,

  h2Size: 40,
  h2Line: 40,

  h3Size: 30,
  h3Line: 40,

  h4Size: 23,
  h4Line: 28,

  h5Size: 20,
  h5Line: 30,

  h6Size: 16,
  h6Line: 22,

  m1: '#75BD23',
  m2: '#A0DA07',

  boundary: 1300,

  gutter: 30,
  push: 30,

  silkDrawerDisappears: '600px',
  silkTableExpanded: '800px',

  mini: '300px',
  tiny: '400px',
  small: '500px',
  medium: '600px',
  big: '700px',
  large: '800px',
  huge: '900px',
  wooser: '1000px',
  yatterman: '1100px',
  gamagori: '1200px',
  aldnoah: '1300px',
  penguindrum: '1400px'

}
